package com.javier.test.step4;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.javier.test.R;

import org.joda.time.DateTime;

import java.util.ArrayList;
import io.realm.Realm;

public class EmployeeViewer extends AppCompatActivity {

    private final int REQUEST_CODE = 200;

    private RecyclerView employeeList;
    private EmployeeAdapter adapter;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_viewer);
        employeeList = findViewById(R.id.employeeList);
        employeeList.setLayoutManager(new LinearLayoutManager(this));
        realm = Realm.getDefaultInstance();
        loadAdapter();
    }

    private void loadAdapter(){
        ArrayList<Employee> employees = new ArrayList<>();
        employees.addAll(realm.copyFromRealm(realm.where(Employee.class).findAll()));
        adapter = new EmployeeAdapter(employees);
        employeeList.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        if(realm != null){
            realm.close();
        }
        super.onDestroy();
    }

    /**
     * Delete employee.
     * @param employee
     */
    private void deleteEmployee(Employee employee){
        if(!realm.isInTransaction()){
            realm.beginTransaction();
        }

        try {
            realm.where(Employee.class)
                    .equalTo("name", employee.getName())
                    .equalTo("job", employee.getJob())
                    .equalTo("birthdate", employee.getBirthdate())
                    .findFirst().deleteFromRealm();

            realm.commitTransaction();
            Toast.makeText(this, R.string.deleted_employee_success, Toast.LENGTH_SHORT).show();
            // Reload data or notify changes.
            loadAdapter();
        }catch (Exception e){
            realm.cancelTransaction();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == REQUEST_CODE){
            // Updated
            loadAdapter();
        }
    }

    /**
     * Employees adapter, can be replaced by realm RecyclerView adapter
     * @see <a href="https://github.com/realm/realm-android-adapters">Realm RecyclerView adapter</a>
     */
    class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.EmployeeHolder>{

        private ArrayList<Employee> employees;

        public EmployeeAdapter(ArrayList<Employee> employees) {
            this.employees = employees;
        }

        @NonNull
        @Override
        public EmployeeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new EmployeeHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.employee_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull EmployeeHolder holder, int position) {
            Employee employee = employees.get(position);
            holder.lbName.setText(employee.getName());
            holder.lbJob.setText(employee.getJob());
            holder.lbBirthdate.setText(new DateTime(employee.getBirthdate().getTime()).toString("dd/MM/yyyy"));
            holder.btDelete.setOnClickListener(view -> deleteEmployee(employee));
            holder.item.setOnClickListener(view -> startActivityForResult(new Intent(EmployeeViewer.this, Step4Activity.class)
                    .putExtra("UPDATE", true)
                    .putExtra("uuid", employee.getUuid()), REQUEST_CODE));
        }

        @Override
        public int getItemCount() {
            return employees.size();
        }

        class EmployeeHolder extends RecyclerView.ViewHolder{

            private TextView lbName;
            private TextView lbJob;
            private TextView lbBirthdate;
            private ImageView btDelete;
            private LinearLayout item;

            public EmployeeHolder(@NonNull View itemView) {
                super(itemView);
                lbName = itemView.findViewById(R.id.lbName);
                lbJob = itemView.findViewById(R.id.lbJob);
                lbBirthdate = itemView.findViewById(R.id.lbBirthdate);
                btDelete = itemView.findViewById(R.id.btDelete);
                item = itemView.findViewById(R.id.item);
            }
        }
    }
}
