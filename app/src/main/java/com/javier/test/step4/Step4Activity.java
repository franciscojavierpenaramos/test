package com.javier.test.step4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.javier.test.R;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import io.realm.Realm;

public class Step4Activity extends AppCompatActivity {

    private TextInputLayout textInputLayout1;
    private TextInputLayout textInputLayout2;
    private TextInputEditText txtEmployeeName;
    private TextInputEditText txtJob;
    private DatePicker datePicker;
    private int mode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step4);
        txtEmployeeName = findViewById(R.id.txtEmployeeName);
        txtJob = findViewById(R.id.txtJob);
        datePicker = findViewById(R.id.datePicker);
        textInputLayout1 = findViewById(R.id.textInputLayout1);
        textInputLayout2 = findViewById(R.id.textInputLayout2);
        Bundle b = getIntent().getExtras();
        if(b != null){
            if(b.getBoolean("UPDATE")){
                mode = 1;
                Realm realm = Realm.getDefaultInstance();
                Employee employeeToUpdate = realm.copyFromRealm(Objects.requireNonNull(realm.where(Employee.class)
                        .equalTo("uuid", b.getString("uuid")).findFirst()));

                if(employeeToUpdate != null){
                    DateTime dateTime = new DateTime(employeeToUpdate.getBirthdate().getTime());
                    txtEmployeeName.setText(employeeToUpdate.getName());
                    txtJob.setText(employeeToUpdate.getJob());
                    datePicker.init(dateTime.getYear(),
                            dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), null);
                }else {
                    mode = 0;
                }
                realm.close();
            }
        }
    }

    /**
     * View all employees.
     * @param v
     */
    public void viewAll(View v){
        startActivity(new Intent(this, EmployeeViewer.class));
    }

    /**
     * Save element.
     * @param v
     */
    public void save(View v){
        // Validate data.
        String employeeName = txtEmployeeName.getText().toString();
        String employeeJob = txtJob.getText().toString();

        Date date = DateTime.parse(datePicker.getDayOfMonth() + "/"
                + datePicker.getMonth() + "/" + datePicker.getYear(), DateTimeFormat.forPattern("dd/MM/yyyy")).toDate();

        textInputLayout1.setError(null);
        textInputLayout2.setError(null);

        if(employeeName.isEmpty()){
            textInputLayout1.setError(getString(R.string.empty_employee_name_validation));
        }

        if(employeeJob.isEmpty()){
            textInputLayout2.setError(getString(R.string.employee_job_validation));
        }

        // Start transaction.
        Realm realm = Realm.getDefaultInstance();
        if(!realm.isInTransaction()){
            realm.beginTransaction();
        }
        try{
            // Create record.
            Employee employee = new Employee();
            // If is update
            if(mode == 1){
                // Get Employee
                employee = realm.where(Employee.class)
                        .equalTo("uuid", getIntent().getExtras().getString("uuid")).findFirst();
            }else {
                employee.setUuid(UUID.randomUUID().toString());
            }

            employee.setName(employeeName);
            employee.setJob(employeeJob);
            employee.setBirthdate(date);
            if(mode == 1){
                realm.copyToRealmOrUpdate(employee);
            }else {
                realm.copyToRealm(employee);
            }
            realm.commitTransaction();
            // Restart inputs.
            txtEmployeeName.getText().clear();
            txtJob.getText().clear();
            if(mode == 1){
                setResult(RESULT_OK);
                finish();
            }else {
                Toast.makeText(this, R.string.employee_save_success, Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            realm.cancelTransaction();
            e.printStackTrace();
        }finally {
            realm.close();
        }
    }
}
