package com.javier.test.request;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.javier.test.R;

import java.util.ArrayList;

public class LocationModelAdapter extends RecyclerView.Adapter<LocationModelAdapter.LocationHolder>{

    private ArrayList<LocationModel> locations;

    public LocationModelAdapter(ArrayList<LocationModel> locations) {
        this.locations = locations;
    }

    @NonNull
    @Override
    public LocationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LocationHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_model_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LocationHolder holder, int position) {
        LocationModel locationModel = locations.get(position);
        holder.lbName.setText(locationModel.getName());
        holder.lbAddress.setText(locationModel.getAddress());
    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

    class LocationHolder extends RecyclerView.ViewHolder{

        private TextView lbName;
        private TextView lbAddress;

        public LocationHolder(@NonNull View itemView) {
            super(itemView);
            lbName = itemView.findViewById(R.id.lbName);
            lbAddress = itemView.findViewById(R.id.lbAddress);
        }
    }
}
