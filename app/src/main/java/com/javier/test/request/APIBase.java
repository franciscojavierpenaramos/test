package com.javier.test.request;

import retrofit2.Retrofit;

public class APIBase {
    public static Retrofit get(){
        return new Retrofit.Builder()
                .baseUrl("http://74.205.41.248:8081/")
                .build();
    }
}
