package com.javier.test.request;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface APIService {

    @FormUrlEncoded
    @POST("pruebawebservice/api/mob_sp_GetArchivo")
    Call<ResponseBody> getZipInfo(@Field("serviceId") int serviceId,
                                   @Field("userId") int userId);

    @GET
    Call<ResponseBody> downloadZIP(@Url String zipURL);

}
