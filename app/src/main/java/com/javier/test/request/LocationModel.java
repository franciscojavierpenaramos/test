package com.javier.test.request;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class LocationModel extends RealmObject {
    @SerializedName("FIIDUBICACION")
    private int id;
    @SerializedName("FIIDCODIGOPOSTAL")
    private int pobox;
    @SerializedName("FCNOMBRE")
    private String name;
    @SerializedName("FCCLAVEUBICACION")
    private String key;
    @SerializedName("FCDIRECCION")
    private String address;
    @SerializedName("FNLATITUD")
    private double latitude;
    @SerializedName("FNLONGITUD")
    private double longitude;
    @SerializedName("FIGEOCERCA")
    private double geofence;
    @SerializedName("FIIDESTATUS")
    private int status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPobox() {
        return pobox;
    }

    public void setPobox(int pobox) {
        this.pobox = pobox;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getGeofence() {
        return geofence;
    }

    public void setGeofence(double geofence) {
        this.geofence = geofence;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
