package com.javier.test.step3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.javier.test.R;

public class Step3Activity extends AppCompatActivity {

    private MaterialButton materialButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_3);
        LinearLayout linearLayout = findViewById(R.id.main);
        materialButton = new MaterialButton(this);
        materialButton.setText(getString(R.string.start_timer));
        materialButton.setOnClickListener(view -> startTimer(() -> runOnUiThread(() -> {
            materialButton.setEnabled(true);
            Toast.makeText(Step3Activity.this, R.string.end_time, Toast.LENGTH_SHORT).show();
        })));
        linearLayout.addView(materialButton);
    }

    /**
     * Timer.
     * @param expirationCallback
     */
    private void startTimer(ExpirationCallback expirationCallback){
        materialButton.setEnabled(false);
        new Thread(() -> {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(expirationCallback != null){
                expirationCallback.onExpirate();
            }
        }).start();
    }

    /**
     * Callback for timer.
     */
    interface ExpirationCallback{
        void onExpirate();
    }
}
