package com.javier.test;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.javier.test.request.LocationModel;
import com.javier.test.step3.Step3Activity;
import com.javier.test.step4.Step4Activity;

import java.util.Iterator;
import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private String TAG = MainActivity.class.getSimpleName();
    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    // Used for download new coordinates.
    private static final int REQUEST_NEW_DATA = 35;

    private static final Pattern cPattern
            = Pattern.compile("^([-+]?)([\\d]{1,2})(((\\.)(\\d+)(,)))(\\s*)(([-+]?)([\\d]{1,3})((\\.)(\\d+))?)$");

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private Realm realm;

    private MaterialButton btAdd;
    private TextInputLayout textInputLayout;
    private TextInputEditText txtLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textInputLayout = findViewById(R.id.textInputLayout);
        txtLatLng = findViewById(R.id.txtLatLng);
        btAdd = findViewById(R.id.btAdd);
        btAdd.setEnabled(false);

        // Realm
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .schemaVersion(1)
                // Added for simplicity.
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(configuration);
        // Start Realm instance
        realm = Realm.getDefaultInstance();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        txtLatLng.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // Check if is empty to clear error in input.
                if(editable.toString().isEmpty()){
                    textInputLayout.setError(null);
                    return;
                }

                // Validate if coordinates are valid.
                if (isLatLngValid(editable.toString())) {
                    textInputLayout.setError(null);
                    btAdd.setEnabled(true);
                } else {
                    btAdd.setEnabled(false);
                    textInputLayout.setError(getString(R.string.invalid_coordinates));
                }
            }
        });

        // Check necessary permissions.
        if(!checkPermissions()){
            requestPermissions();
        }
    }

    // Add any valid coordinates as a marker.
    public void addLatLng(View v){
        String coordinates = txtLatLng.getText().toString();
        if(isLatLngValid(coordinates)){
            // Remove error message.
            textInputLayout.setError(null);
            LatLng latLng = extractLatLng(coordinates);
            // Add marker.
            mMap.addMarker(new MarkerOptions().position(latLng)
                    .title(coordinates));
            // Move camera.
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            // Restart input.
            txtLatLng.getText().clear();
        }else {
            // Show error.
            textInputLayout.setError(getString(R.string.invalid_coordinates));
        }
    }

    /**
     * Open step 3
     * @param v
     */
    public void onStep3(View v){
        startActivity(new Intent(this, Step3Activity.class));
    }

    /**
     * Open step 4
     * @param v
     */
    public void onStep4(View v){
        startActivity(new Intent(this, Step4Activity.class));
    }

    /**
     * Start import coordinates.
     * @param v
     */
    public void importData(View v){
        startActivityForResult(new Intent(MainActivity.this, RequestActivity.class), REQUEST_NEW_DATA);
    }

    /**
     * Extract LatLng from String.
     * @param coordinates
     * @return
     */
    private LatLng extractLatLng(String coordinates){
        String[] latlngString = coordinates.trim().split(",");
        return new LatLng(Double.parseDouble(latlngString[0]), Double.parseDouble(latlngString[1]));
    }

    /**
     * Check if coordinates are valid.
     * @param coordinates
     * @return
     */
    private boolean isLatLngValid(String coordinates){
        return cPattern.matcher(coordinates).matches();
    }

    @Override
    protected void onDestroy() {
        // Close Realm instance
        if(realm != null){
            realm.close();
        }
        super.onDestroy();
    }

    /**
     * Returns the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        return  PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(R.id.activity_main),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.permission_rationale_accept, view -> {
                        // Request permissionv
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_PERMISSIONS_REQUEST_CODE);
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);

        btAdd.setEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == REQUEST_NEW_DATA){
            // Load from database.
            if(realm == null || realm.isClosed()){
                realm = Realm.getDefaultInstance();
            }

            Iterator<LocationModel> locations = realm.where(LocationModel.class).findAll().iterator();
            while (locations.hasNext()){
                LocationModel model = locations.next();
                mMap.addMarker(new MarkerOptions().position(new LatLng(model.getLatitude(), model.getLongitude()))
                        .title(String.format("%s\n%s", model.getName(), model.getAddress())));
            }
        }
    }
}
