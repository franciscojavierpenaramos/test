package com.javier.test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.javier.test.request.APIBase;
import com.javier.test.request.APIService;
import com.javier.test.request.LocationModel;
import com.javier.test.request.LocationModelAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();
    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private final int serviceId = 55;
    private final int userId = 12984;
    private CompositeDisposable disposable;
    private ContentLoadingProgressBar progress;

    private RecyclerView coordinatesList;
    private LocationModelAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        disposable = new CompositeDisposable();
        setContentView(R.layout.activity_request);
        findViewById(R.id.btBack).setOnClickListener(view -> finish());
        coordinatesList = findViewById(R.id.coordinatesList);
        coordinatesList.setLayoutManager(new LinearLayoutManager(this));
        progress = findViewById(R.id.progress);
        progress.hide();
    }

    /**
     * Download ZIP.
     * @param zipURL
     */
    private void downloadZip(String zipURL){
        APIService service = APIBase.get().create(APIService.class);
        service.downloadZIP(zipURL).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    ResponseBody body = response.body();
                    if(body != null) {
                        Disposable d = Observable.create(emitter -> {
                            try{
                                emitter.onNext(writeFile(body));
                            }catch (Exception e){
                                e.printStackTrace();
                                if(!emitter.isDisposed()) {
                                    emitter.onError(e);
                                }
                            }
                        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(o -> {
                            // Set adapter
                            adapter = new LocationModelAdapter((ArrayList<LocationModel>) o);
                            coordinatesList.setAdapter(adapter);
                            progress.hide();
                        }, throwable -> {
                            progress.hide();
                            throwable.printStackTrace();
                            Toast.makeText(RequestActivity.this, "Ocurrió un error al guardar el archivo.", Toast.LENGTH_SHORT).show();
                        });

                        disposable.add(d);
                    }

                } else {
                    Toast.makeText(RequestActivity.this, getString(R.string.error_to_download_file), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                progress.hide();
            }
        });
    }

    /**
     * Write ZIP.
     * @param body
     * @throws IOException
     */
    private ArrayList<LocationModel> writeFile(ResponseBody body) throws IOException {
        int count;
        byte data[] = new byte[1024 * 4];
        long fileSize = body.contentLength();
        InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "coordinates.zip");
        OutputStream output = new FileOutputStream(outputFile);
        long total = 0;
        long startTime = System.currentTimeMillis();
        int timeCount = 1;
        while ((count = bis.read(data)) != -1) {

            total += count;
            double current = Math.round(total / (Math.pow(1024, 2)));

            int progress = (int) ((total * 100) / fileSize);
            System.out.println(progress);

            long currentTime = System.currentTimeMillis() - startTime;

            if (currentTime > 1000 * timeCount) {
                timeCount++;
            }

            output.write(data, 0, count);
        }
        output.flush();
        output.close();
        bis.close();

        // Unzip file
        unzip(outputFile);
        return loadCoordinatesToDB();
    }

    private ArrayList<LocationModel> loadCoordinatesToDB(){
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "coordinates.txt");
        ArrayList<LocationModel> locationModels = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        // Start transaction.
        if(!realm.isInTransaction()){
            realm.beginTransaction();
        }
        // Delete old data.
        realm.where(LocationModel.class).findAll().deleteAllFromRealm();
        try {
            FileInputStream stream = new FileInputStream(file);
            String jsonStr = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

                jsonStr = Charset.defaultCharset().decode(bb).toString();
            }
            catch(Exception e){
                e.printStackTrace();
            }
            finally {
                stream.close();
            }

            JSONObject jsonObj = new JSONObject(jsonStr);
            JSONArray mainData = jsonObj.getJSONArray("data")
                    .getJSONObject(0).getJSONArray("UBICACIONES");

            Gson gson = new Gson();
            for(int i = 0; i < mainData.length(); i++){
                LocationModel locationModelItem = gson.fromJson(mainData.getJSONObject(i).toString(), LocationModel.class);
                Log.i(TAG, locationModelItem.getName());
                locationModels.add(locationModelItem);
                realm.copyToRealm(locationModelItem);
            }

            realm.commitTransaction();
        } catch (Exception e) {
            realm.cancelTransaction();
            e.printStackTrace();
        }finally {
            realm.close();
        }

        return locationModels;
    }

    /**
     * Unzip file
     * @param zipFile
     * @throws IOException
     */
    public static void unzip(File zipFile) throws IOException {
        ZipInputStream zis = new ZipInputStream(
                new BufferedInputStream(new FileInputStream(zipFile)));
        try {
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "coordinates.txt");
                File dir = ze.isDirectory() ? file : file.getParentFile();
                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Failed to ensure directory: " +
                            dir.getAbsolutePath());
                if (ze.isDirectory())
                    continue;
                FileOutputStream fout = new FileOutputStream(file);
                try {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                } finally {
                    fout.close();
                }
            }
        } finally {
            zis.close();
        }
    }

    /**
     * Start request.
     * @param v
     */
    public void download(View v){
        if(!checkPermissions()){
            requestPermissions();
            return;
        }

        progress.show();

        // Example: [{"code":1,"hasError":"ok","valueResponse":"http://softvibes.com.mx/MA/cargaInicial_2019-03-15_12984.zip"}]
        Disposable d = get().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(responseBody -> {
            JSONArray data = new JSONArray(responseBody.string());
            if(data.length() == 0){
                Toast.makeText(RequestActivity.this, getString(R.string.no_data_found), Toast.LENGTH_SHORT).show();
                progress.hide();
                return;
            }

            JSONObject main = data.getJSONObject(0);
            if(main.has("valueResponse")){
                downloadZip(main.getString("valueResponse"));
            }else {
                progress.hide();
                Toast.makeText(RequestActivity.this, getString(R.string.invalid_format), Toast.LENGTH_SHORT).show();
            }
        });
        disposable.add(d);
    }


    /**
     * Get main info.
     * @return
     */
    public Observable<ResponseBody> get(){
        return Observable.create(emitter -> {
            try{
                APIService service = APIBase.get().create(APIService.class);
                Call<ResponseBody> responseBodyCall = service.getZipInfo(serviceId, userId);
                emitter.onNext(responseBodyCall.execute().body());
            }catch (Exception e){
                e.printStackTrace();
                if(!emitter.isDisposed()) {
                    emitter.onError(e);
                }
            }
        });
    }



    @Override
    protected void onDestroy() {
        if(disposable != null){
            disposable.dispose();
        }
        super.onDestroy();
    }

    /**
     * Add all records to map.
     * @param v
     */
    public void addToMap(View v){
        setResult(RESULT_OK);
        finish();
    }

    /**
     * Returns the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        return  PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(R.id.activity_main),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.permission_rationale_accept, view -> {
                        // Request permissionv
                        ActivityCompat.requestPermissions(RequestActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_PERMISSIONS_REQUEST_CODE);
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(RequestActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                download(null);
            }
        }
    }
}
